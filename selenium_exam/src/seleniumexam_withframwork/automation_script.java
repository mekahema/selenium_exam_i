package seleniumexam_withframwork;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;


public class automation_script extends Reusablemethods {
	//public static WebDriver driver;
	//private static String result;
	
	public static void iteam_add(){
		try{
			startReport("SF_TestCase_1","D:/Selenium Prctice/Report/","chrome");
			
			String dt_path = "D:/Selenium Prctice/Amazon_testcases/Testdata/Testdata.xlsx";
			String Or_path = "D:/Selenium Prctice/Amazon_testcases/Testdata/Amazon_ObjectRepository.xlsx";
		        readlocators(Or_path);
				readtestdata(dt_path);
			
				
			
	     for(int i=1;i<data.length;i++){
	    	 String driverdata=data[i][5];
	     String url=data[i][1];
	     String searchdata=data[i][2];
	     String comapringdata=data[i][3];
	     String expected=data[i][4];
	     
	     Browserlaunch(driverdata);
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().window().maximize();
	     
		

			

			setvalue(1);
			By bysearching =getpath(locatortype,value);
		WebElement ele_search = driver.findElement(bysearching);
		enterText(ele_search,searchdata ,obj_name);
		
		setvalue(2);
		By byclick_button =getpath(locatortype,value);
		WebElement ele_searchbutton=driver.findElement(byclick_button);
		clickButton(ele_searchbutton, obj_name);
		
		setvalue(3);
		By byouter_box_of_iphones =getpath(locatortype,value);
		WebElement ele_box_of_iphones=driver.findElement(byouter_box_of_iphones);
		
		setvalue(4);
		By bybox_of_lists =getpath(locatortype,value);
		List<WebElement> ele_bybox_of_lists=driver.findElements(bybox_of_lists);
		
		for(int j=0;j<ele_bybox_of_lists.size();j++){
			System.out.println(ele_bybox_of_lists.get(i).getText());
			if(ele_bybox_of_lists.get(i).getText().contains(comapringdata)){
				ele_bybox_of_lists.get(i).click();
				break;
			}
			
		}
		setvalue(5);
		By byaddingcart=getpath(locatortype,value);
		WebElement ele_addingcart=driver.findElement(byaddingcart);
		clickButton(ele_addingcart, obj_name);
		
		setvalue(6);
		By byclick_popup=getpath(locatortype,value);
		WebElement ele_click_popup=driver.findElement(byclick_popup);
		clickButton(ele_click_popup, obj_name);
		
		setvalue(7);
		By bygetdatafromcart=getpath(locatortype,value);
		WebElement ele_getdatafromcart=driver.findElement(bygetdatafromcart);
		String actual=gettext(ele_getdatafromcart);
		
	
		//expected
		if(expected.equals(actual)){
		
		Update_Report("Pass"," iteam_add()","Cart value matches to adding value", driver); 
		
		}else{
			Update_Report("Fail","iteam_add()","Cart value  not matches to adding value", driver);
         }
		
		driver.close();
			}
	     
		}catch(Exception e){
			System.out.println(e);
		}
	}
		public static void Dropdown_links(){
		try{
			startReport("SF_TestCase_1","D:/Selenium Prctice/Report/","chrome");
			
			String dt_path = "D:/Selenium Prctice/Amazon_testcases/Testdata/Testdata2.xlsx";
			String Or_path = "D:/Selenium Prctice/Amazon_testcases/Testdata/Amazon_ObjectRepository.xlsx";
		        readlocators(Or_path);
				readtestdata(dt_path);
			
				
			
	     for(int i=1;i<data.length;i++){
	    	 String driverdata=data[i][4];
	     String url=data[i][1];
	     String expected=data[i][2];
	     String expected1=data[i][3];
	    // String expected=data[i][4];
	     
	     Browserlaunch(driverdata);
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().window().maximize();
	     
			setvalue(8);
			By bydropdown =getpath(locatortype,value);
		WebElement ele_dropdown = driver.findElement(bydropdown);
		Actions act=new Actions(driver);
		act.click(ele_dropdown).build().perform();
		//WebElement box = driver.findElement(By.xpath("//div[@class='nav-template nav-flyout-content nav-tpl-itemList']"));
		
		setvalue(9);
		By byamazonlink =getpath(locatortype,value);
	WebElement ele_byamazonlink = driver.findElement(byamazonlink);
	String actual=gettext(ele_byamazonlink);
	
	
	//expected
	if(expected.equals(actual)){
	
	Update_Report("Pass"," iteam_add()","its dislays correct link on home page", driver); 
	
	}else{
		Update_Report("Fail","iteam_add()","home page link is not matching to input data", driver);
     }
		
		
	setvalue(10);
	By bytodayslink =getpath(locatortype,value);
WebElement ele_todayslink = driver.findElement(byamazonlink);
String actual1=gettext(ele_todayslink);

	

	if(expected1.equals(actual1)){
		Update_Report("Pass"," iteam_add()","its dislays correct link on home page", driver); 
		
	}else{
		Update_Report("Fail","iteam_add()","home page link is not matching to input data", driver);
     }
	
	     
	driver.close();
			}
	     
		}catch(Exception e){
			System.out.println(e);
		}
		}
		public static void Dropdown_validation(){
			try{
				startReport("SF_TestCase_1","D:/Selenium Prctice/Report/","chrome");
				
				String dt_path = "D:/Selenium Prctice/Amazon_testcases/Testdata/Testdata3.xlsx";
				String Or_path = "D:/Selenium Prctice/Amazon_testcases/Testdata/Amazon_ObjectRepository1.xlsx";
			        readlocators(Or_path);
					readtestdata(dt_path);
				
					
				
		     for(int i=1;i<data.length;i++){
		    	 String driverdata=data[i][2];
		     String url=data[i][1];
		    // String expected=data[i][2];
		    // String expected1=data[i][3];
		    // String expected=data[i][4];
		     
		     Browserlaunch(driverdata);
				driver.get(url);
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().window().maximize();
		     
				setvalue(8);
				By bydropdown =getpath(locatortype,value);
			WebElement ele_dropdown = driver.findElement(bydropdown);
		

		
		Actions act=new Actions(driver);
		act.moveToElement(ele_dropdown).build().perform();
		setvalue(11);
		By byboxdropdown =getpath(locatortype,value);
	WebElement ele_boxdropdown = driver.findElement(byboxdropdown);
	
	setvalue(12);
	By byboxdropdownlinks =getpath(locatortype,value);
	List<WebElement> ele_boxdropdownlinks = ele_boxdropdown.findElements(byboxdropdownlinks);
		
	
		for(int k=1;k<ele_boxdropdownlinks.size();k++){
			String names = ele_boxdropdownlinks.get(k).getText();
			
			
		}
		     
		driver.close();
				}
		     
			}catch(Exception e){
				System.out.println(e);
			}
		}
	
		public static void signaccounttab_validation(){
			
			try{
				startReport("SF_TestCase_1","D:/Selenium Prctice/Report/","chrome");
				
				String dt_path = "D:/Selenium Prctice/Amazon_testcases/Testdata/Testdata4.xlsx";
				String Or_path = "D:/Selenium Prctice/Amazon_testcases/Testdata/Amazon_ObjectRepository.xlsx";
			        readlocators(Or_path);
					readtestdata(dt_path);		
				
		     for(int i=1;i<data.length;i++){
		    	 String driverdata=data[i][2];
		     String url=data[i][1];
		     //String expected=data[i][2];
		    // String expected1=data[i][3];
		    // String expected=data[i][4];
		     
		     Browserlaunch(driverdata);
				driver.get(url);
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().window().maximize();
		     
				setvalue(13);
				By bymenubarsignin =getpath(locatortype,value);
			WebElement ele_menubarsignin = driver.findElement(bymenubarsignin);
		Actions act=new Actions(driver);
		act.moveToElement(ele_menubarsignin).build().perform();
		
		setvalue(14);
		By bymenubarsigninbox =getpath(locatortype,value);
	WebElement ele_menubarsigninbox = driver.findElement(bymenubarsignin);
		
	setvalue(15);
	By bymenubarsigninboxlist =getpath(locatortype,value);
	List<WebElement> ele_menubarsigninboxlist = ele_menubarsigninbox.findElements(bymenubarsignin);
		
		for(int l=1;l<ele_menubarsigninboxlist.size();l++){
			String names = ele_menubarsigninboxlist.get(l).getText();
			System.out.println(names);
			
		}
	}
driver.close();
			
	     
		}catch(Exception e){
			System.out.println(e);
		}
	}
}


		
	


