package seleniumexam_withframwork;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.junit.Test;



public class Reusablemethods extends driver {
	static String[][] xlData;
	static String[][] data;
	 static String[][] locator;
	 static String obj_name;
	 static String locatortype;
	 static String value;

	//	static WebDriver driver;
	static BufferedWriter bw = null;
	static BufferedWriter bw1 = null;
	static String htmlname;
	static String objType;
	static String objName;
	static String TestData;
	static String rootPath;
	static int report;


	static Date cur_dt = null;
	static String filenamer;
	static String TestReport;
	int rowcnt;
	static String exeStatus = "True";
	static int iflag = 0;
	static int j = 1;

	static String fireFoxBrowser;
	static String chromeBrowser;

	static String result;

	static int intRowCount = 0;
	static String dataTablePath;
	static int i;
	static String browserName;


	public static WebDriver driver;
	
	public static WebDriver Browserlaunch(String browsertype){
		if(browsertype.equals("mozilla"))
		{
	      return driver=new FirefoxDriver();
		}else if(browsertype.equals("chrome")){
			System.setProperty("WebDriver.Chrome.driver", "c:/Seleniumdriver/chromedriver.exe");
			return driver=new ChromeDriver();
		}else if(browsertype.equals("ie")){
			System.setProperty("WebDriver.ie.driver", "c:/Seleniumdriver/IEserverdriver.exe");
			return driver=new InternetExplorerDriver();
		}
		return driver=null;
	}
		
		
	//}
	/* getting data from object repository */
	public static void setvalue(int row){
		obj_name=locator[row][1];
		locatortype=locator[row][2];
		value=locator[row][3];
	}
	
	/* Object Repository implementation 
	 * */
	public static By getpath(String type, String value){
		
		switch(type){
		case "id":
			return By.id(value);
		case "name":
			return By.name(value);
		case "tagName":
		    return By.tagName(value);
		case "linkText":
			return By.linkText(value);
		case "partialLinktext":
			return By.partialLinkText(value);
		case "class":
		    return By.className(value);
		case "xpath":
			return By.xpath(value);
		case "cssSelector":
			return By.cssSelector(value);
		default: 
			System.out.println("unknown type");
		    return null;
		}
		
	}
	
	public static void enterText(WebElement obj, String textVal, String objName) throws IOException{
		if(obj.isDisplayed()){
			obj.sendKeys(textVal);
			System.out.println("Pass: " + textVal + " is entered in " + objName +" field");
			Update_Report("Pass", "enterText",textVal + " is entered in " + objName +" field",driver); 
			//Update_Report("pass","enterText","  textVal + is entered in " + objName +" field" , driver);
		}else{
			System.out.println("Fail: " + objName + " field is not displayed,please check your application");
			//Update_Report("Fail","enterText",objName + " field is not displayed,please check your application" , driver);
		}
	}


/*clciking the button*/
public static void clickButton(WebElement obj,  String objName){
		
		if(obj.isDisplayed()){
			obj.click();
			System.out.println("Pass: " + objName +" is clicked.");
		}else{
			System.out.println("Fail: " + objName + " field is not displayed,please check your application");

		}
		
	}
//get text data
public static String gettext(WebElement msg){
	String text=null;
	 text=msg.getText();
	return text;
}
//clear text box
public static void clear(WebElement textbox){
	textbox.clear();
}
//compare actual and expected values
public static String verify(String expectedvalue, String actualvalue){
	if(expectedvalue.equals(actualvalue)){
		result="pass";
		return result;
	}else{
		result="fail";
	return result;
	}
}

public   static String[][] readtestdata(String path) throws IOException{
	 data=readXlSheet(path);
	return data;
}
public static void readlocators(String path) throws IOException{
	locator=readXlSheet(path);
}
	
/*Exceldata reader reusablemethod*/
	
public static String[][] readXlSheet(String dt_path) throws IOException
 {
//try 
// {
		
		File xlFile = new File(dt_path);

		FileInputStream xlDoc = new FileInputStream(xlFile);

         XSSFWorkbook xwb = new XSSFWorkbook(xlDoc);
		//XSSFSheet sheet = wb.getSheet(sheetName);
        // ArrayList<String> listofsheets= new ArrayList<String>();
         
         // retrieve all the sheets in a file
        // for(int i=0;i<xwb.getNumberOfSheets();i++){
        //	 listofsheets.add(xwb.getSheetName(i));
        // }
       //Iterate through each sheet and retrieves the data and stores it in an arraylist
        // for(String s:listofsheets)
        // {
        	 XSSFSheet sheet = xwb.getSheet("Sheet1"); 
        	 int iRowCount = sheet.getLastRowNum()+1;
     		int iColCount = sheet.getRow(0).getLastCellNum();

     		 xlData = new String[iRowCount][iColCount];
     		System.out.println(iRowCount+"iRowCount");
     		System.out.println(iColCount+"iColCount");
     		 
         
		for(int i = 1; i< iRowCount; i++)
		{
			for(int j = 1; j<iColCount; j++)
			{
				XSSFCell cell = sheet.getRow(i).getCell(j);
			//if(cell!=null)
			//{
				xlData[i][j]= cell.getStringCellValue();
				System.out.println(xlData[i][j]);
			//}
		  }

	   }
     //}
         //xwb.close();
   
//}
//catch(Exception e)
//{
	
//}
return xlData;
}

/* For Reports start and update Reports */

public static void startReport(String scriptName, String ReportsPath,String browserName) throws IOException{
	j =0;
	String strResultPath = null;
	String testScriptName =scriptName;

	cur_dt = new Date(); 
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
	String strTimeStamp = dateFormat.format(cur_dt);

	if (ReportsPath == "") { 

		ReportsPath = "C:\\";
	}

	if (ReportsPath.endsWith("\\")) { 
		ReportsPath = ReportsPath + "\\";
	}

	strResultPath = ReportsPath + "Log" + "/" +testScriptName +"/"; 
	File f = new File(strResultPath);
	f.mkdirs();
	htmlname = strResultPath  + testScriptName + "_" + strTimeStamp 
			+ ".html";

	bw = new BufferedWriter(new FileWriter(htmlname));

	bw.write("<HTML><BODY><TABLE BORDER=0 CELLPADDING=3 CELLSPACING=1 WIDTH=100%>");
	bw.write("<TABLE BORDER=0 BGCOLOR=BLACK CELLPADDING=3 CELLSPACING=1 WIDTH=100%>");
	bw.write("<TR><TD BGCOLOR=#66699 WIDTH=27%><FONT FACE=VERDANA COLOR=WHITE SIZE=2><B>Browser Name</B></FONT></TD><TD COLSPAN=6 BGCOLOR=#66699><FONT FACE=VERDANA COLOR=WHITE SIZE=2><B>"
			+ browserName + "</B></FONT></TD></TR>");
	bw.write("<HTML><BODY><TABLE BORDER=1 CELLPADDING=3 CELLSPACING=1 WIDTH=100%>");
	bw.write("<TR COLS=7><TD BGCOLOR=#BDBDBD WIDTH=3%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>SL No</B></FONT></TD>"
			+ "<TD BGCOLOR=#BDBDBD WIDTH=10%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Step Name</B></FONT></TD>"
			+ "<TD BGCOLOR=#BDBDBD WIDTH=10%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Execution Time</B></FONT></TD> "
			+ "<TD BGCOLOR=#BDBDBD WIDTH=10%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Status</B></FONT></TD>"
			+ "<TD BGCOLOR=#BDBDBD WIDTH=47%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Detail Report</B></FONT></TD></TR>");


}
/*
 * Name of the Method: Update_Report
 * Brief description : Updates HTML report with test results
 * Arguments: Res_type:holds the response of test script,Action:Action performed,result:contains test results
 * Created by: Automation team
 * Creation date : July 17 2017
 * last modified:  July 17 2017
 */


public static void Update_Report(String Res_type,String Action, String result,WebDriver dr) throws IOException {
	Date exec_time = new Date();
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
	String str_time = dateFormat.format(exec_time);
	
	if (Res_type.startsWith("Pass")) {
		//String ss1Path= screenshot(dr);
		
		bw.write("<TR COLS=7><TD BGCOLOR=#EEEEEE WIDTH=3%><FONT FACE=VERDANA SIZE=2>"
				+ (j++)
				+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=10%><FONT FACE=VERDANA SIZE=2>"
				+Action
				+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=10%><FONT FACE=VERDANA SIZE=2>"
				+ str_time
				+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=10%><FONT FACE=VERDANA SIZE=2 COLOR = GREEN>"
				+ "Passed"
				+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=30%><FONT FACE=VERDANA SIZE=2 COLOR = GREEN>"
				+ result + "</FONT></TD></TR>");

	} else if (Res_type.startsWith("Fail")) {
		//To generate report in only single file
		
		String ss1Path= screenshot(dr);
		exeStatus = "Failed";
		report = 1;
		bw.write("<TR COLS=7><TD BGCOLOR=#EEEEEE WIDTH=3%><FONT FACE=VERDANA SIZE=2>"
				+ (j++)
				+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=10%><FONT FACE=VERDANA SIZE=2>"
				+Action
				+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=10%><FONT FACE=VERDANA SIZE=2>"
				+ str_time
				+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=10%><FONT FACE=VERDANA SIZE=2 COLOR = RED>"
				+ "<a href= "
				+ ss1Path
				
				+ "  style=\"color: #FF0000\"> Failed </a>"

					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=30%><FONT FACE=VERDANA SIZE=2 COLOR = RED>"
					+ result + "</FONT></TD></TR>");


	} 
}

public static String screenshot(WebDriver dr) throws IOException{
	
	Date exec_time = new Date();
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
	String str_time = dateFormat.format(exec_time);
	//String fileName = "C:/Users/Sreeram/Desktop/WorkDayScreenShots/ss.png";
	String  ss1Path = "D:/Selenium Prctice/Amazon_testcases/Reports"+str_time+".png";
	File scrFile = ((TakesScreenshot)dr).getScreenshotAs(OutputType.FILE);
	FileUtils.copyFile(scrFile, new File(ss1Path));
	return ss1Path;
}

}



