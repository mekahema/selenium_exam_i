package selenium_exam;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class testcase10_search {
	public static WebDriver driver;
	public static void main(String[] args) {
		driver =new FirefoxDriver();
		driver.get("https://www.amazon.com/");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		WebElement search = driver.findElement(By.id("twotabsearchtextbox"));
		search.sendKeys("iphone");
		String firstdropdown = driver.findElement(By.id("issDiv0")).getText();
		if(firstdropdown.equals("iphone charger")){
			System.out.println("firstdropdown is displayed with"+firstdropdown);
		}
		String seconddropdown = driver.findElement(By.id("issDiv1")).getText();
		if(seconddropdown.equals("in electronics")){
			System.out.println("firstdropdown is displayed with"+seconddropdown);
		}
		String thriddropdown = driver.findElement(By.id("issDiv2")).getText();
		if(thriddropdown.equals("Cell Phones & Accessories")){
			System.out.println("firstdropdown is displayed with"+thriddropdown);
		}
		
	}

}
