package selenium_exam;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class signin_menubar_tc4 {
	public static WebDriver driver;
	public static void main(String[] args) {
		driver =new FirefoxDriver();
		driver.get("https://www.amazon.com/");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		WebElement menubarsignin = driver.findElement(By.xpath("//a[@id='nav-link-accountList']"));
		Actions act=new Actions(driver);
		act.moveToElement(menubarsignin).build().perform();
		
		WebElement box = driver.findElement(By.xpath("//div[@id='nav-flyout-accountList']"));
		List<WebElement> links = box.findElements(By.tagName("a"));
		System.out.println(links.size());
		for(int i=1;i<links.size();i++){
			String names = links.get(i).getText();
			System.out.println(names);
			
		}
	}

}
