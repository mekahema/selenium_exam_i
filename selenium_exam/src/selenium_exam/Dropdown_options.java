package selenium_exam;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class Dropdown_options {
	public static WebDriver driver;
	public static void main(String[] args) {
		driver =new FirefoxDriver();
		driver.get("https://www.amazon.com/");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		WebElement dropdown = driver.findElement(By.xpath("//span[text()='Departments']"));
		Actions act=new Actions(driver);
		act.moveToElement(dropdown).build().perform();
		
		WebElement box = driver.findElement(By.xpath("//div[@class='nav-template nav-flyout-content nav-tpl-itemList']"));
		List<WebElement> links = box.findElements(By.xpath("//div[@class='nav-template nav-flyout-content nav-tpl-itemList']/span"));
		System.out.println(links.size());
		for(int i=1;i<links.size();i++){
			String names = links.get(i).getText();
			System.out.println(names);
			
		}
	}
}
