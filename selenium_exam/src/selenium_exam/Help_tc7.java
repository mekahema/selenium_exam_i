package selenium_exam;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Help_tc7 {
	public static WebDriver driver;
	public static void main(String[] args) {
		driver =new FirefoxDriver();
		driver.get("https://www.amazon.com/");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	   driver.findElement(By.linkText("Help")).click();
		WebElement box = driver.findElement(By.xpath("//div[@class='a-section a-spacing-large ss-landing-container-wide']"));
List<WebElement> helplinks = box.findElements(By.tagName("a"));
for(int i=0;i<helplinks.size();i++){
	String linknames = helplinks.get(i).getText();
	System.out.println(linknames);
}
WebElement searchbox = driver.findElement(By.id("helpsearch"));
if(searchbox.isDisplayed()){
	System.out.println("find more solution textbox had displayed on help page");
}else{
	System.out.println("find more solution textbox wouldn't displayed on help page");
}
	}

}
